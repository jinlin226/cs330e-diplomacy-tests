#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/Testdiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve


# -----------
# Testdiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        l = diplomacy_read(s)
        self.assertEqual(l, [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'],
                             ['C', 'London', 'Support', 'B']])

    def test_read_1(self):
        s = StringIO("A Washington Move Austin\nB Austin Support D\nC London Support A\nD Houston Move London\n")
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Washington', 'Move', 'Austin'], ['B', 'Austin', 'Support', 'D'],
                             ['C', 'London', 'Support', 'A'], ['D', 'Houston', 'Move', 'London']])

    def test_read__2(self):
        s = StringIO("A London Move Paris\nB Paris Support A\n")
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'London', 'Move', 'Paris'], ['B', 'Paris', 'Support', 'A']])

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Washington Move Austin\nB Austin Support D\nC London Support A\nD Houston Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch Testdiplomacy.py >  Testdiplomacy.out 2>&1


$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.001s
OK


$ coverage report -m                   >> Testdiplomacy.out



$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.001s
OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          57      0     32      0   100%
TestDiplomacy.py      39      0      0      0   100%
--------------------------------------------------------------
TOTAL                 96      0     32      0   100%

"""
